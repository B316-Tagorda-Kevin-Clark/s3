package com.zuitt.activity;
import java.util.Scanner;


public class activity {
    public  static void main(String[] args){

        Scanner input = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed:");

        int integer = input.nextInt();
        long result = 1;

        for(int i = 1; i <= integer; i++){
            result *= i;
        }

        System.out.println("The factorial of " + integer + " is " + result);



        int rows = 5;

        for(int i = 1; i <=rows; i++){
            for(int x = 1; x <= i; x++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
